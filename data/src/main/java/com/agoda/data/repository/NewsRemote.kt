package com.agoda.data.repository

import com.agoda.data.model.NewsItem
import io.reactivex.Observable

interface NewsRemote {
    fun getNews(): Observable<List<NewsItem>>
}