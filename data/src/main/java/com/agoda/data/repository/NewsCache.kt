package com.agoda.data.repository

import com.agoda.data.model.NewsItem
import entities.NewsEntity
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single

interface NewsCache {

    fun clearNews(): Completable

    fun saveNews(News: List<NewsItem>): Completable

    fun getNews(): Observable<List<NewsItem>>

    fun isCached(): Single<Boolean>

    fun setLastCacheTime(lastCache: Long)

    fun isExpired(): Single<Boolean>
}