package com.agoda.data.model

/**
 * This represents a news item
 */
data class NewsItem(
    val title: String = "",
    val summary: String = "",
    val articleUrl: String = "",
    val byline: String = "",
    val publishedDate: String = "",
    val mediaList: List<MediaItem> = listOf()
)