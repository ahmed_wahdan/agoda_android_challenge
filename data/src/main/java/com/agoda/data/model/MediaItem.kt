package com.agoda.data.model

class MediaItem(
    val url: String,
    val format: String,
    val height: Int,
    val width: Int,
    val type: String,
    val subType: String,
    val caption: String,
    val copyright: String
)
