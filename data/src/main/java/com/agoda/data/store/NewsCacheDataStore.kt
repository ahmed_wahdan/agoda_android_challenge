 package com.agoda.data.store

    import com.agoda.data.model.NewsItem
    import com.agoda.data.repository.NewsCache
    import com.agoda.data.repository.NewsDataStore
    import io.reactivex.Completable
    import io.reactivex.Observable
    import io.reactivex.Single

 open class NewsCacheDataStore constructor(
        private val newsCache: NewsCache
    ) :
    NewsDataStore {
        override fun isCached(): Single<Boolean> {
            return newsCache.isCached()
        }

        override fun getNews(): Observable<List<NewsItem>> {
        return newsCache.getNews()
    }

    override fun saveNews(News: List<NewsItem>): Completable {
        return newsCache.saveNews(News)
                .doOnComplete {
                    newsCache.setLastCacheTime(System.currentTimeMillis()) }
    }

    override fun clearNews(): Completable {
        return newsCache.clearNews()
    }
}