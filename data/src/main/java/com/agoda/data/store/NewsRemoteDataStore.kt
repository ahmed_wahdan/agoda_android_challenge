package com.agoda.data.store

import com.agoda.data.model.NewsItem
import com.agoda.data.repository.NewsDataStore
import com.agoda.data.repository.NewsRemote
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single

open class NewsRemoteDataStore constructor(
    private val newsRemote: NewsRemote
) :
    NewsDataStore {

    override fun isCached(): Single<Boolean> {
        throw UnsupportedOperationException("Checking cache isn't supported here...")
    }

    override fun getNews(): Observable<List<NewsItem>> {
        return newsRemote.getNews()
    }
    override fun saveNews(News: List<NewsItem>): Completable {
        throw UnsupportedOperationException("Saving News isn't supported here...")
    }

    override fun clearNews(): Completable {
        throw UnsupportedOperationException("Clearing News isn't supported here...")
    }
}
