package com.agoda.data.store

import com.agoda.data.repository.NewsDataStore

open class NewsDataStoreFactory constructor(
    private val newsCacheDataStore: NewsCacheDataStore,
    private val newsRemoteDataStore: NewsRemoteDataStore
) {

    open fun getDataStore(
        newsCached: Boolean,
        cacheExpired: Boolean
    ): NewsDataStore {
        return if (newsCached && !cacheExpired) {
            newsCacheDataStore
        } else {
            newsRemoteDataStore
        }
    }

    open fun getCacheDataStore(): NewsDataStore {
        return newsCacheDataStore
    }

    fun getRemoteDataStore(): NewsDataStore {
        return newsRemoteDataStore
    }
}