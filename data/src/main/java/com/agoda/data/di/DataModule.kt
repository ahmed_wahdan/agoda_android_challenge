package com.agoda.data.di

import com.agoda.data.NewsDataRepository
import com.agoda.data.mapper.MediaItemMapper
import com.agoda.data.mapper.NewsMapper
import com.agoda.data.repository.NewsDataStore
import com.agoda.data.store.NewsCacheDataStore
import com.agoda.data.store.NewsDataStoreFactory
import com.agoda.data.store.NewsRemoteDataStore
import com.agoda.domain.interactor.GetNewsUseCase
import org.koin.core.qualifier.Qualifier
import org.koin.core.qualifier.named
import org.koin.dsl.module

val dataModule = module(override=true) {
    factory<NewsDataStore>(named("remote")) { NewsRemoteDataStore(get()) }
    factory<NewsDataStore>(named("local")) { NewsCacheDataStore(get()) }
    factory { NewsDataStoreFactory(get(named("local")), get(named("remote"))) }
    single { NewsDataRepository(get(),get(),get()) }
    factory { NewsMapper(MediaItemMapper()) }
}

