package com.agoda.data

import com.agoda.data.mapper.NewsMapper
import com.agoda.data.repository.NewsCache
import com.agoda.data.store.NewsDataStoreFactory
import entities.NewsEntity
import io.reactivex.Observable
import io.reactivex.functions.BiFunction
import repository.NewsRepository

class NewsDataRepository constructor(
    private val mapper: NewsMapper,
    private val cache: NewsCache,
    private val factory: NewsDataStoreFactory
) :
    NewsRepository {

    override fun getNews(): Observable<List<NewsEntity>> {
        return Observable.zip(cache.isCached().toObservable(),
                cache.isExpired().toObservable(),
                BiFunction<Boolean, Boolean, Pair<Boolean, Boolean>> { areCached, isExpired ->
                    Pair(areCached, isExpired)
                })
                .flatMap {
                    factory.getDataStore(it.first, it.second).getNews()
                }
                .flatMap { News ->
                    factory.getCacheDataStore()
                            .saveNews(News)
                            .andThen(Observable.just(News))
                }
                .map { it ->
                    it.map {
                        mapper.mapToEntity(it)
                    }
                }
    }
}