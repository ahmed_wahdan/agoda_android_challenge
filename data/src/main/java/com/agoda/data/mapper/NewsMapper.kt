package com.agoda.data.mapper

import com.agoda.data.model.NewsItem
import entities.NewsEntity

class NewsMapper constructor(val mediaItemMapper: MediaItemMapper) : EntityMapper<NewsEntity, NewsItem> {
    override fun mapFromEntity(domain: NewsEntity): NewsItem {
        return NewsItem(
            domain.title,
            domain.summary,
            domain.articleUrl,
            domain.byline,
            domain.publishedDate,
            domain.mediaEntityList.map { mediaItemMapper.mapFromEntity(domain = it) }
        )
    }

    override fun mapToEntity(data: NewsItem): NewsEntity {
    return NewsEntity(
        data.title,
        data.summary,
        data.articleUrl,
        data.byline,
        data.publishedDate,
        data.mediaList.map { mediaItemMapper.mapToEntity(data = it) }
        )
    }
}