package com.agoda.data.mapper

import com.agoda.data.model.MediaItem
import entities.MediaEntity

class MediaItemMapper : EntityMapper<MediaEntity, MediaItem> {

    override fun mapFromEntity(domain: MediaEntity): MediaItem {
        return MediaItem(
            domain.url,
            domain.format,
            domain.height,
            domain.width,
            domain.type,
            domain.subType,
            domain.caption,
            domain.copyright
        )
    }

    override fun mapToEntity(data: MediaItem): MediaEntity {
      return MediaEntity(
          data.url,
          data.format,
          data.height,
          data.width,
          data.type,
          data.subType,
          data.caption,
          data.copyright
        )
    }
}