package com.agoda.data.mapper

interface EntityMapper<E, D> {

    fun mapFromEntity(domain: E): D

    fun mapToEntity(data: D): E
}