package com.agoda

object Dependencies {

    val kotlin_stdlib = "org.jetbrains.kotlin:kotlin-stdlib-jdk8:${Versions.kotlinVersion}"
    val kotlin_gradle_plugin = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.kotlinVersion}"
    val android_gradle_plugin = "com.android.tools.build:gradle:${Versions.android_gradle_plugin}"

    object Network {
        const val okhttp = "com.squareup.okhttp3:okhttp:${Versions.Retrofit.okHttp}"
        const val okhttpLoggingInterceptor = "com.squareup.okhttp3:logging-interceptor:${Versions.Retrofit.okHttp}"
        const val retrofit = "com.squareup.retrofit2:retrofit:${Versions.Retrofit.retrofit}"
        const val retrofitMoshi = "com.squareup.retrofit2:converter-moshi:${Versions.Retrofit.moshi}"
        const val retrofitRxAdapter = "com.squareup.retrofit2:adapter-rxjava2:${Versions.Retrofit.retrofit}"
    }

    object Rx {
        const val kotlin = "io.reactivex.rxjava2:rxkotlin:${Versions.rxKotlinVersion}"
        const val java = "io.reactivex.rxjava2:rxjava:${Versions.rxJavaVersion}"
    }

    object Koin {
        const val core = "org.koin:koin-core:${Versions.koinVersion}"
        const val android = "org.koin:koin-android:${Versions.koinVersion}"
        const val viewModel = "org.koin:koin-androidx-viewmodel:${Versions.koinVersion}"
        const val Test = "org.koin:koin-test:${Versions.koinVersion}"
    }

    object Lint {
        const val kotlin = "org.jmailen.gradle:kotlinter-gradle:${Versions.kotlinVersion}"
    }

    object Testing {
        const val junit = "org.jetbrains.kotlin:kotlin-test-junit:${Versions.kotlinVersion}"
        const val mockitoCore = "org.mockito:mockito-core:${Versions.Testing.mockitoVersion}"
        const val mockitoForKotlin =
            "com.nhaarman.mockitokotlin2:mockito-kotlin:${Versions.Testing.mockitoForKotlinVersion}"
    }

    object Room {
        const val roomRuntime = "android.arch.persistence.room:runtime:${Versions.Room.version}"
        const val roomCompiler = "android.arch.persistence.room:compiler:${Versions.Room.version}"
        const val roomRxJava = "android.arch.persistence.room:rxjava2:${Versions.Room.version}"
    }
}