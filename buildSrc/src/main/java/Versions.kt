package com.agoda
object Versions {

    const val kotlinVersion = "1.3.31"
    const val android_gradle_plugin = "3.4.1"
    const val  kotlinter = "1.26.0"
    const val koinVersion = "2.0.0-rc-2"
    const val rxJavaVersion = "2.2.9"
    const val rxKotlinVersion = "2.3.0"

    object Retrofit {
        const val retrofit = "2.3.0"
        const val okHttp = "3.11.0"
        const val  moshi = "2.4.0"
    }
    object Testing {
         const val mockitoVersion = "2.8.47"
         const val mockitoForKotlinVersion = "2.1.0"

    }
    object  Room{
       const val version = "1.1.1"
    }
}