package com.agoda.remote.mapper

import com.agoda.data.model.MediaItem
import com.agoda.remote.model.MultimediaItem

class MediaItemMapper : EntityMapper<MultimediaItem, MediaItem> {
    override fun mapFromRemote(remote: MultimediaItem): MediaItem {
        return MediaItem(
            remote.url ?: "",
            remote.format ?: "",
            remote.height ?: 0,
            remote.width ?: 0,
            remote.type ?: "",
            remote.subtype ?: "",
            remote.caption ?: "",
            remote.copyright ?: ""
        )
    }
}