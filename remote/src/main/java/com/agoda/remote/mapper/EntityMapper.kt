package com.agoda.remote.mapper

interface EntityMapper<in M, out E> {
    fun mapFromRemote(remote: M): E
}