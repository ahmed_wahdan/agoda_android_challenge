package com.agoda.remote.mapper

import com.agoda.data.model.NewsItem
import com.agoda.remote.model.ResultsItem

class NewsMapper constructor(val mediaItemMapper: MediaItemMapper) : EntityMapper<ResultsItem, NewsItem> {
    override fun mapFromRemote(remote: ResultsItem): NewsItem {
        return NewsItem((remote.title ?: ""),
        remote.jsonMemberAbstract ?: "",
        remote.url ?: "",
        remote.byline ?: "",
        remote.publishedDate ?: "",
        remote.multimedia?.map { mediaItemMapper.mapFromRemote(remote = it) } ?: listOf()
        ) }
}