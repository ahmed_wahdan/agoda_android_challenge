package com.agoda.remote

import com.agoda.data.model.NewsItem
import com.agoda.data.repository.NewsRemote
import com.agoda.remote.mapper.NewsMapper
import com.agoda.remote.service.NewsService
import io.reactivex.Observable

class NewsRemoteImp constructor(
    private val newsService: NewsService,
    private val newsMapper: NewsMapper
) : NewsRemote {

    override fun getNews(): Observable<List<NewsItem>> {
         return newsService.getNews().map {
                it.results?.map { it -> newsMapper.mapFromRemote(it) }
            }
    }
}