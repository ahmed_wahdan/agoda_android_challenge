package com.agoda.remote.model

import javax.annotation.Generated
import com.squareup.moshi.Json

@Generated("com.robohorse.robopojogenerator")
data class NewsResponse(

    @Json(name = "copyright")
    val copyright: String? = null,

    @Json(name = "last_updated")
    val lastUpdated: String? = null,

    @Json(name = "section")
    val section: String? = null,

    @Json(name = "results")
    val results: List<ResultsItem>? = null,

    @Json(name = "num_results")
    val numResults: Int? = null,

    @Json(name = "status")
    val status: String? = null
)