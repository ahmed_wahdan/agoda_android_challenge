package com.agoda.remote.model

import javax.annotation.Generated
import com.squareup.moshi.Json

@Generated("com.robohorse.robopojogenerator")
data class MultimediaItem(

    @Json(name = "copyright")
    val copyright: String? = null,

    @Json(name = "subtype")
    val subtype: String? = null,

    @Json(name = "format")
    val format: String? = null,

    @Json(name = "width")
    val width: Int? = null,

    @Json(name = "caption")
    val caption: String? = null,

    @Json(name = "type")
    val type: String? = null,

    @Json(name = "url")
    val url: String? = null,

    @Json(name = "height")
    val height: Int? = null
)