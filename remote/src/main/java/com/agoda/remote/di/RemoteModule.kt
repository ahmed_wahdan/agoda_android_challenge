package com.agoda.remote.di

import com.agoda.remote.NewsRemoteImp
import com.agoda.remote.mapper.MediaItemMapper
import com.agoda.remote.mapper.NewsMapper
import com.agoda.remote.network.NewsServiceFactory
import org.koin.dsl.module

val remoteModule = module(override = true) {
    factory { NewsMapper(MediaItemMapper()) }
    factory { NewsServiceFactory.makeNewsService(true) }
    factory { NewsRemoteImp(get(), get()) }
}
