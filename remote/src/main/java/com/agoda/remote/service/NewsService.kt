package com.agoda.remote.service

import com.agoda.remote.model.NewsResponse
import io.reactivex.Observable
import retrofit2.http.GET

interface NewsService {

    @GET("bins/nl6jh/")
    fun getNews(): Observable<NewsResponse>
}