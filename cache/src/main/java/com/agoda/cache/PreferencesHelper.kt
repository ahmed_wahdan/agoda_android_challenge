package com.agoda.cache

import android.content.Context
import android.content.SharedPreferences

open class PreferencesHelper(context: Context) {

    private val bufferPref: SharedPreferences by  lazy {
        context.getSharedPreferences("com.agoda.news", Context.MODE_PRIVATE)
    }
    var lastCacheTime: Long
        get() = bufferPref.getLong("CACH_KEY", 0)
        set(lastCache) = bufferPref.edit().putLong("CACH_KEY", lastCache).apply()

}