package com.agoda.cache.mapper

interface EntityMapper<T, V> {

    fun mapFromCached(cache: T): V

    fun mapToCached(data: V): T
}

