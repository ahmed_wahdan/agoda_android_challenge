package com.agoda.cache.mapper

import com.agoda.cache.model.NewsItem

class NewsMapper:EntityMapper<NewsItem,com.agoda.data.model.NewsItem> {
    override fun mapFromCached(cache: NewsItem): com.agoda.data.model.NewsItem {
        return com.agoda.data.model.NewsItem(
            cache.title,
            cache.summary,
            cache.articleUrl,
            cache.byline,
            cache.publishedDate,
            listOf())

    }

    override fun mapToCached(data: com.agoda.data.model.NewsItem): NewsItem {
        return NewsItem(
            data.title,
            data.summary,
            data.articleUrl,
            data.byline,
            data.publishedDate
        )
    }
}