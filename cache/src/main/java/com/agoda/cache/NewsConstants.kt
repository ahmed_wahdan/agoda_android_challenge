package com.agoda.cache

object NewsConstants {
    const val TABLE_NAME = "news"

    const val QUERY_NEWS = "SELECT * FROM" + " " + TABLE_NAME

    const val DELETE_ALL_NEWS = "DELETE FROM" + " " + TABLE_NAME
}
