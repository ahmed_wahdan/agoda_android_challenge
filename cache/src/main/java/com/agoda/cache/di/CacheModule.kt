package com.agoda.cache.di

import androidx.room.Room
import com.agoda.cache.NewsCacheImp
import com.agoda.cache.PreferencesHelper
import com.agoda.cache.db.AgodaNewsDatabase
import com.agoda.cache.mapper.NewsMapper
import org.koin.dsl.module
import org.koin.android.ext.koin.androidContext

val cacheModule = module(override=true) {
    single { PreferencesHelper(androidContext()) }
    factory { NewsMapper() }
    single { Room.databaseBuilder(androidContext(),
        AgodaNewsDatabase::class.java, "agoda_news.db")
        .build() }
    factory { NewsCacheImp(get(), get(),get()) }

}