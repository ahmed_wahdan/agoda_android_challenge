package com.agoda.cache

import com.agoda.cache.db.AgodaNewsDatabase
import com.agoda.cache.mapper.NewsMapper
import com.agoda.data.model.NewsItem
import com.agoda.data.repository.NewsCache
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single

class NewsCacheImp constructor(private val agodaNewsDatabase: AgodaNewsDatabase,
                               private val newsMapper: NewsMapper, private val preferencesHelper: PreferencesHelper
) : NewsCache {
    private val EXPIRATION_TIME = (60 * 10 * 1000).toLong()

    override fun clearNews(): Completable {
        return Completable.defer {
            agodaNewsDatabase.cachedNewsDao().clearNews()
            Completable.complete()
        }
    }

    override fun saveNews(news: List<NewsItem>): Completable {
        return Completable.defer {
            news.forEach {
                agodaNewsDatabase.cachedNewsDao().insertNews(
                    newsMapper.mapToCached(it))
            }
            Completable.complete()
        }
    }

    override fun getNews(): Observable<List<NewsItem>> {
        return Observable.defer {
            Observable.just(agodaNewsDatabase.cachedNewsDao().getNews())
        }.map {
            it.map { newsMapper.mapFromCached(it) }
        }
    }

    override fun isCached(): Single<Boolean> {
        return Single.defer {
            Single.just(agodaNewsDatabase.cachedNewsDao().getNews().isNotEmpty())
        }
    }

    override fun setLastCacheTime(lastCache: Long) {
        preferencesHelper.lastCacheTime = lastCache
    }

    override fun isExpired(): Single<Boolean> {
        return Single.defer {
            val currentTime = System.currentTimeMillis()
            val lastUpdateTime = preferencesHelper.lastCacheTime
            Single.just(
             currentTime - lastUpdateTime > EXPIRATION_TIME
            )
        }
    }


}