package com.agoda.cache.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.agoda.cache.dao.CachedNewsDao
import com.agoda.cache.model.NewsItem

@Database(entities = [NewsItem::class], version = 1)
abstract class AgodaNewsDatabase : RoomDatabase() {

    abstract fun cachedNewsDao(): CachedNewsDao

    private var INSTANCE: AgodaNewsDatabase? = null

    private val sLock = Any()

    fun getInstance(context: Context): AgodaNewsDatabase {
        if (INSTANCE == null) {
            synchronized(sLock) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.applicationContext,
                        AgodaNewsDatabase::class.java, "agoda_task.db")
                            .build()
                }
                return INSTANCE!!
            }
        }
        return INSTANCE!!
    }

}