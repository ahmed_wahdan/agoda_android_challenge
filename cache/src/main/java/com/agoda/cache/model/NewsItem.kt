package com.agoda.cache.model

import androidx.room.Entity
import com.agoda.cache.NewsConstants

/**
 * This represents a news item
 */
@Entity(tableName = NewsConstants.TABLE_NAME)
data class NewsItem(
    val title: String = "",
    val summary: String = "",
    val articleUrl: String = "",
    val byline: String = "",
    val publishedDate: String = ""
)