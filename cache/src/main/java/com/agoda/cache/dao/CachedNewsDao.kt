package com.agoda.cache.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.agoda.cache.NewsConstants
import com.agoda.cache.model.NewsItem

@Dao
abstract class CachedNewsDao {

    @Query(NewsConstants.QUERY_NEWS)
    abstract fun getNews(): List<NewsItem>

    @Query(NewsConstants.DELETE_ALL_NEWS)
    abstract fun clearNews()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertNews(cachedNews: NewsItem)

}