package com.agoda.domain.interactor

import com.agoda.domain.base.PostExecutionThread
import com.agoda.domain.base.ObservableUseCase
import entities.NewsEntity
import io.reactivex.Observable
import repository.NewsRepository

class GetNewsUseCase(
    private val newsRepository: NewsRepository,
    postExecutionThread: PostExecutionThread
) : ObservableUseCase<List<NewsEntity>, Nothing>(postExecutionThread) {
    public override fun buildUseCase(params: Nothing?): Observable<List<NewsEntity>> {
       return newsRepository.getNews()
    }
}