package com.agoda.domain.base

import io.reactivex.Scheduler

interface PostExecutionThread {
    val scheduler: Scheduler
}