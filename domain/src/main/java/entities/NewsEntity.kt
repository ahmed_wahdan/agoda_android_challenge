package entities

/**
 * This represents a news item
 */
data class NewsEntity(
    val title: String,
    val summary: String,
    val articleUrl: String,
    val byline: String,
    val publishedDate: String,
    val mediaEntityList: List<MediaEntity>
)