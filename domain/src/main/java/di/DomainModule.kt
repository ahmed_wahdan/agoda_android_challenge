package di

import com.agoda.domain.interactor.GetNewsUseCase
import org.koin.dsl.module

val domainModule = module {
    factory { GetNewsUseCase(get(),get()) }
}