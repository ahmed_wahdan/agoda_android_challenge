package repository

import entities.NewsEntity
import io.reactivex.Observable

interface NewsRepository {
    fun getNews(): Observable<List<NewsEntity>>
}