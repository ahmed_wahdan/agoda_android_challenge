package com.agoda.domain.interactor

import com.agoda.domain.base.PostExecutionThread
import com.nhaarman.mockitokotlin2.whenever
import entities.NewsEntity
import io.reactivex.Observable
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import repository.NewsRepository

internal class GetNewsUseCaseTest {
    private lateinit var getNewsUseCase: GetNewsUseCase
    @Mock
    lateinit var newsRepository: NewsRepository
    @Mock lateinit var postExecutionThread: PostExecutionThread

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        getNewsUseCase = GetNewsUseCase(newsRepository, postExecutionThread)
    }
    @Test
    fun getNewsCompletes() {
        stubNewsRepositoryGetNews(
            Observable.just(NewsDataFactory.makeNeswsList(2)))

        val testObserver = getNewsUseCase.buildUseCase().test()
        testObserver.assertComplete()
    }
    @Test
    fun getProjectsCallsRepository() {
        stubNewsRepositoryGetNews(
            Observable.just(NewsDataFactory.makeNeswsList(2)))

        getNewsUseCase.buildUseCase().test()
        verify(newsRepository).getNews()
    }

    @Test
    fun getProjectsReturnsData() {
        val projects = NewsDataFactory.makeNeswsList(2)
        stubNewsRepositoryGetNews(
            Observable.just(projects))

        val testObserver = getNewsUseCase.buildUseCase().test()
        testObserver.assertValue(projects)
    }
    private fun stubNewsRepositoryGetNews(observable: Observable<List<NewsEntity>>) {
        whenever(newsRepository.getNews())
            .thenReturn(observable)
    }
}