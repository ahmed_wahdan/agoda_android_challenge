import entities.NewsEntity

object NewsDataFactory {

    fun randomUuid(): String {
        return java.util.UUID.randomUUID().toString()
    }

    fun makeNewsItem(): NewsEntity {
        return NewsEntity(randomUuid(), randomUuid(), randomUuid(), randomUuid(),
                randomUuid(), listOf()
        )
    }

    fun makeNeswsList(count: Int) : List<NewsEntity> {
        val newsEntity = mutableListOf<NewsEntity>()
        repeat(count) {
            newsEntity.add(makeNewsItem())
        }
        return newsEntity
    }

}